const arr = ['Jakarta', 'Bandung', 'Semarang', 'Bali','Indonesia Raya']

const length = function(any) {
    return any.length
}

//This code for count length of array's element
console.log(arr.map(x => x.length))

//This code for filter how many elements more than 4
console.log(arr.map(x => x.length).filter(x => x > 4).length)

//This code for sort from depend on the length of elements
console.log(
    arr.sort(function(a,b){
        return b.length - a.length
    })
)